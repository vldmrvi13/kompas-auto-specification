# -*- coding: utf-8 -*-
#|testkompas

import pythoncom
from win32com.client import Dispatch, gencache

import random
import LDefin2D
import MiscellaneousHelpers as MH

import os

#  Подключим константы API Компас
kompas6_constants = gencache.EnsureModule("{75C9F5D0-B5B8-4526-8681-9903C567D2ED}", 0, 1, 0).constants
kompas6_constants_3d = gencache.EnsureModule("{2CAF168C-7961-4B90-9DA2-701419BEEFE3}", 0, 1, 0).constants

#  Подключим описание интерфейсов API5
kompas6_api5_module = gencache.EnsureModule("{0422828C-F174-495E-AC5D-D31014DBBE87}", 0, 1, 0)
kompas_object = kompas6_api5_module.KompasObject(Dispatch("Kompas.Application.5")._oleobj_.QueryInterface(kompas6_api5_module.KompasObject.CLSID, pythoncom.IID_IDispatch))
MH.iKompasObject  = kompas_object

#  Подключим описание интерфейсов API7
kompas_api7_module = gencache.EnsureModule("{69AC2981-37C0-4379-84FD-5DD2F3C0A520}", 0, 1, 0)
application = kompas_api7_module.IApplication(Dispatch("Kompas.Application.7")._oleobj_.QueryInterface(kompas_api7_module.IApplication.CLSID, pythoncom.IID_IDispatch))
MH.iApplication  = application


Documents = application.Documents
#  Получим активный документ
kompas_document = application.ActiveDocument
kompas_document_spc = kompas_api7_module.ISpecificationDocument(kompas_document)
iDocumentSpc = kompas_object.SpcActiveDocument()

# Получаю путь к файлу(Таблица должна лежать в той же паке где скрипт)
put = os.getcwd()
print(put)

# Подключение к Excel и выбор файла
Excel = Dispatch('Excel.Application')
ex = Excel.Workbooks.Open(put + '\\test.xlsx') #Имя исходного файла
#listEx = ex.ActiveSheet

#Логика построения структуры
i = 1
c = 1
elements = 22 #количество элементов списка
for elment_count in range(1,elements):
    #print(listEx.Cells(int(elment_count),2).value)
    #print(elment_count)
    #print(i)
    #print(c)
    if str(listEx.Cells(elment_count,2).value) == str(listEx.Cells(elment_count + 1,2).value):
        #print(i)
        #print(c)
        i += 1
    #счетчик количества элементов группы
    else:
        print(i)
        if i > 1:
            #print(i)
            lable = str(listEx.Cells(elment_count,2).value)
            lable_filt = lable.replace("None","")
            iSpc = iDocumentSpc.GetSpecification()
            iSpc.ksSpcObjectCreate("", 0, 20, 0, 0, 0)
            obj = iSpc.ksSpcObjectEnd()
            iSpcObjParam = kompas6_api5_module.ksSpcObjParam(kompas_object.GetParamStruct(kompas6_constants.ko_SpcObjParam))
            iSpc.ksSpcObjectEdit(obj)

            iDocumentSpc.ksGetObjParam(obj, iSpcObjParam, LDefin2D.ALLPARAM)
            iDocumentSpc.ksSetObjParam(obj, iSpcObjParam, LDefin2D.ALLPARAM)
            iTextLineArray = kompas_object.GetDynamicArray(LDefin2D.TEXT_LINE_ARR)
            #Наименование
            iTextLineParam = kompas6_api5_module.ksTextLineParam(kompas_object.GetParamStruct(kompas6_constants.ko_TextLineParam))
            iTextLineParam.Init()
            iTextLineParam.style = 0
            iTextItemArray = kompas_object.GetDynamicArray(LDefin2D.TEXT_ITEM_ARR)
            iTextItemParam = kompas6_api5_module.ksTextItemParam(kompas_object.GetParamStruct(kompas6_constants.ko_TextItemParam))
            iTextItemParam.Init()
            iTextItemParam.iSNumb = 0
            iTextItemParam.s = lable_filt
            iTextItemParam.type = 0
            iTextItemFont = kompas6_api5_module.ksTextItemFont(iTextItemParam.GetItemFont())
            iTextItemFont.Init()
            iTextItemFont.bitVector = 4096
            iTextItemFont.color = 0
            iTextItemFont.fontName = "GOST type A"
            iTextItemFont.height = 5
            iTextItemFont.ksu = 1
            iTextItemArray.ksAddArrayItem(-1, iTextItemParam)
            iTextLineParam.SetTextItemArr(iTextItemArray)

            iTextLineArray.ksAddArrayItem(-1, iTextLineParam)
            iSpc.ksSetSpcObjectColumnTextEx(5, 1, 0, iTextLineArray)
            iTextLineArray = kompas_object.GetDynamicArray(LDefin2D.TEXT_LINE_ARR)
            obj = iSpc.ksSpcObjectEnd()
            for list_of_elements in range(c , c + i):
                iSpc = iDocumentSpc.GetSpecification()
                iSpc.ksSpcObjectCreate("", 0, 20, 0, 0, 0)
                obj = iSpc.ksSpcObjectEnd()
                iSpcObjParam = kompas6_api5_module.ksSpcObjParam(kompas_object.GetParamStruct(kompas6_constants.ko_SpcObjParam))
                iSpc.ksSpcObjectEdit(obj)

                iDocumentSpc.ksGetObjParam(obj, iSpcObjParam, LDefin2D.ALLPARAM)
                iDocumentSpc.ksSetObjParam(obj, iSpcObjParam, LDefin2D.ALLPARAM)
                iTextLineArray = kompas_object.GetDynamicArray(LDefin2D.TEXT_LINE_ARR)
                position_non = str(listEx.Cells(list_of_elements,1).value)
                name_non = str(listEx.Cells(list_of_elements,3).value)
                quantity_non = str(listEx.Cells(list_of_elements,4).value)
                position_filt = position_non.replace("None","")
                name_filt = name_non.replace("None","")
                quantity_filt = quantity_non.replace("None","")
                quantity_filt = quantity_filt.replace(".0","")

                #Поз. обозначение
                iTextLineParam = kompas6_api5_module.ksTextLineParam(kompas_object.GetParamStruct(kompas6_constants.ko_TextLineParam))
                iTextLineParam.Init()
                iTextLineParam.style = 0
                iTextItemArray = kompas_object.GetDynamicArray(LDefin2D.TEXT_ITEM_ARR)
                iTextItemParam = kompas6_api5_module.ksTextItemParam(kompas_object.GetParamStruct(kompas6_constants.ko_TextItemParam))
                iTextItemParam.Init()
                iTextItemParam.iSNumb = 0
                iTextItemParam.s = position_filt
                iTextItemParam.type = 0
                iTextItemFont = kompas6_api5_module.ksTextItemFont(iTextItemParam.GetItemFont())
                iTextItemFont.Init()
                iTextItemFont.bitVector = 4096
                iTextItemFont.color = 0
                iTextItemFont.fontName = "GOST type A"
                iTextItemFont.height = 5
                iTextItemFont.ksu = 1
                iTextItemArray.ksAddArrayItem(-1, iTextItemParam)
                iTextLineParam.SetTextItemArr(iTextItemArray)

                iTextLineArray.ksAddArrayItem(-1, iTextLineParam)
                iSpc.ksSetSpcObjectColumnTextEx(10, 1, 0, iTextLineArray)
                iTextLineArray = kompas_object.GetDynamicArray(LDefin2D.TEXT_LINE_ARR)

                #Наименование
                iTextLineParam = kompas6_api5_module.ksTextLineParam(kompas_object.GetParamStruct(kompas6_constants.ko_TextLineParam))
                iTextLineParam.Init()
                iTextLineParam.style = 0
                iTextItemArray = kompas_object.GetDynamicArray(LDefin2D.TEXT_ITEM_ARR)
                iTextItemParam = kompas6_api5_module.ksTextItemParam(kompas_object.GetParamStruct(kompas6_constants.ko_TextItemParam))
                iTextItemParam.Init()
                iTextItemParam.iSNumb = 0
                iTextItemParam.s = name_filt
                iTextItemParam.type = 0
                iTextItemFont = kompas6_api5_module.ksTextItemFont(iTextItemParam.GetItemFont())
                iTextItemFont.Init()
                iTextItemFont.bitVector = 4096
                iTextItemFont.color = 0
                iTextItemFont.fontName = "GOST type A"
                iTextItemFont.height = 5
                iTextItemFont.ksu = 1
                iTextItemArray.ksAddArrayItem(-1, iTextItemParam)
                iTextLineParam.SetTextItemArr(iTextItemArray)

                iTextLineArray.ksAddArrayItem(-1, iTextLineParam)
                iSpc.ksSetSpcObjectColumnTextEx(5, 1, 0, iTextLineArray)
                iTextLineArray = kompas_object.GetDynamicArray(LDefin2D.TEXT_LINE_ARR)

                #Количество
                iTextLineParam = kompas6_api5_module.ksTextLineParam(kompas_object.GetParamStruct(kompas6_constants.ko_TextLineParam))
                iTextLineParam.Init()
                iTextLineParam.style = 0
                iTextItemArray = kompas_object.GetDynamicArray(LDefin2D.TEXT_ITEM_ARR)
                iTextItemParam = kompas6_api5_module.ksTextItemParam(kompas_object.GetParamStruct(kompas6_constants.ko_TextItemParam))
                iTextItemParam.Init()
                iTextItemParam.iSNumb = 0
                iTextItemParam.s = quantity_filt
                iTextItemParam.type = 0
                iTextItemFont = kompas6_api5_module.ksTextItemFont(iTextItemParam.GetItemFont())
                iTextItemFont.Init()
                iTextItemFont.bitVector = 4096
                iTextItemFont.color = 0
                iTextItemFont.fontName = "GOST type A"
                iTextItemFont.height = 5
                iTextItemFont.ksu = 1
                iTextItemArray.ksAddArrayItem(-1, iTextItemParam)
                iTextLineParam.SetTextItemArr(iTextItemArray)

                iTextLineArray.ksAddArrayItem(-1, iTextLineParam)
                iSpc.ksSetSpcObjectColumnTextEx(6, 1, 0, iTextLineArray)
                iTextLineArray = kompas_object.GetDynamicArray(LDefin2D.TEXT_LINE_ARR)

                obj = iSpc.ksSpcObjectEnd()

        else:
            #print(i)
            position_non = str(listEx.Cells(elment_count,1).value)
            quantity_non = str(listEx.Cells(elment_count,4).value)
            position_filt = position_non.replace("None","")
            quantity_filt = quantity_non.replace("None","")
            quantity_filt = quantity_filt.replace(".0","")
            b_col = str(listEx.Cells(elment_count,2).value)
            c_col = str(listEx.Cells(elment_count,3).value)
            b_col_filt = b_col.replace("None","")
            c_col_filt = c_col.replace("None","")
            combo_element = b_col_filt + " " + c_col_filt
            iSpc = iDocumentSpc.GetSpecification()
            iSpc.ksSpcObjectCreate("", 0, 20, 0, 0, 0)
            obj = iSpc.ksSpcObjectEnd()
            iSpcObjParam = kompas6_api5_module.ksSpcObjParam(kompas_object.GetParamStruct(kompas6_constants.ko_SpcObjParam))
            iSpc.ksSpcObjectEdit(obj)

            iDocumentSpc.ksGetObjParam(obj, iSpcObjParam, LDefin2D.ALLPARAM)
            iDocumentSpc.ksSetObjParam(obj, iSpcObjParam, LDefin2D.ALLPARAM)
            iTextLineArray = kompas_object.GetDynamicArray(LDefin2D.TEXT_LINE_ARR)

            #Поз. обозначение
            iTextLineParam = kompas6_api5_module.ksTextLineParam(kompas_object.GetParamStruct(kompas6_constants.ko_TextLineParam))
            iTextLineParam.Init()
            iTextLineParam.style = 0
            iTextItemArray = kompas_object.GetDynamicArray(LDefin2D.TEXT_ITEM_ARR)
            iTextItemParam = kompas6_api5_module.ksTextItemParam(kompas_object.GetParamStruct(kompas6_constants.ko_TextItemParam))
            iTextItemParam.Init()
            iTextItemParam.iSNumb = 0
            iTextItemParam.s = position_filt
            iTextItemParam.type = 0
            iTextItemFont = kompas6_api5_module.ksTextItemFont(iTextItemParam.GetItemFont())
            iTextItemFont.Init()
            iTextItemFont.bitVector = 4096
            iTextItemFont.color = 0
            iTextItemFont.fontName = "GOST type A"
            iTextItemFont.height = 5
            iTextItemFont.ksu = 1
            iTextItemArray.ksAddArrayItem(-1, iTextItemParam)
            iTextLineParam.SetTextItemArr(iTextItemArray)

            iTextLineArray.ksAddArrayItem(-1, iTextLineParam)
            iSpc.ksSetSpcObjectColumnTextEx(10, 1, 0, iTextLineArray)
            iTextLineArray = kompas_object.GetDynamicArray(LDefin2D.TEXT_LINE_ARR)

            #Наименование
            iTextLineParam = kompas6_api5_module.ksTextLineParam(kompas_object.GetParamStruct(kompas6_constants.ko_TextLineParam))
            iTextLineParam.Init()
            iTextLineParam.style = 0
            iTextItemArray = kompas_object.GetDynamicArray(LDefin2D.TEXT_ITEM_ARR)
            iTextItemParam = kompas6_api5_module.ksTextItemParam(kompas_object.GetParamStruct(kompas6_constants.ko_TextItemParam))
            iTextItemParam.Init()
            iTextItemParam.iSNumb = 0
            iTextItemParam.s = combo_element
            iTextItemParam.type = 0
            iTextItemFont = kompas6_api5_module.ksTextItemFont(iTextItemParam.GetItemFont())
            iTextItemFont.Init()
            iTextItemFont.bitVector = 4096
            iTextItemFont.color = 0
            iTextItemFont.fontName = "GOST type A"
            iTextItemFont.height = 5
            iTextItemFont.ksu = 1
            iTextItemArray.ksAddArrayItem(-1, iTextItemParam)
            iTextLineParam.SetTextItemArr(iTextItemArray)

            iTextLineArray.ksAddArrayItem(-1, iTextLineParam)
            iSpc.ksSetSpcObjectColumnTextEx(5, 1, 0, iTextLineArray)
            iTextLineArray = kompas_object.GetDynamicArray(LDefin2D.TEXT_LINE_ARR)

            #Количество
            iTextLineParam = kompas6_api5_module.ksTextLineParam(kompas_object.GetParamStruct(kompas6_constants.ko_TextLineParam))
            iTextLineParam.Init()
            iTextLineParam.style = 0
            iTextItemArray = kompas_object.GetDynamicArray(LDefin2D.TEXT_ITEM_ARR)
            iTextItemParam = kompas6_api5_module.ksTextItemParam(kompas_object.GetParamStruct(kompas6_constants.ko_TextItemParam))
            iTextItemParam.Init()
            iTextItemParam.iSNumb = 0
            iTextItemParam.s = quantity_filt
            iTextItemParam.type = 0
            iTextItemFont = kompas6_api5_module.ksTextItemFont(iTextItemParam.GetItemFont())
            iTextItemFont.Init()
            iTextItemFont.bitVector = 4096
            iTextItemFont.color = 0
            iTextItemFont.fontName = "GOST type A"
            iTextItemFont.height = 5
            iTextItemFont.ksu = 1
            iTextItemArray.ksAddArrayItem(-1, iTextItemParam)
            iTextLineParam.SetTextItemArr(iTextItemArray)

            iTextLineArray.ksAddArrayItem(-1, iTextLineParam)
            iSpc.ksSetSpcObjectColumnTextEx(6, 1, 0, iTextLineArray)
            iTextLineArray = kompas_object.GetDynamicArray(LDefin2D.TEXT_LINE_ARR)

            obj = iSpc.ksSpcObjectEnd()
        c += i
        i = 1